import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './services/auth-guard.service';

import { LoginComponent } from './components/login/login.component';
import { SignupComponent } from './components/signup/signup.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { DashboardAlumnoComponent } from './components/dashboard-alumno/dashboard-alumno.component';
import { dashboard_routes } from './components/dashboard/dashboard.routes';
import { dashboard_routesAlumno } from './components/dashboard-alumno/dashboardAlumno.routes';
import {DashboardProfesorComponent} from './components/dashboard-profesor/dashboard-profesor.component';
import {dashboard_routesProfesor} from './components/dashboard-profesor/dashboardProfesor.routes'

const APP_ROUTES:Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    children: dashboard_routes,
    canActivate: [ AuthGuardService ]
  },
   {
    path: 'students',
    component: DashboardAlumnoComponent,
    children: dashboard_routesAlumno,
    canActivate: [ AuthGuardService ]
  },
     {
    path: 'teacher',
    component: DashboardProfesorComponent,
    children: dashboard_routesProfesor,
    canActivate: [ AuthGuardService ]
  },
  { path: '**', pathMatch: 'full', redirectTo:'login'}
]

export const app_routing = RouterModule.forRoot(APP_ROUTES);
