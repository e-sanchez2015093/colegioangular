import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//ROUTES
import { app_routing } from './app.routes';

//SERVICES
import { UsuarioService } from './services/usuario.service';
import { AuthGuardService } from './services/auth-guard.service';
import { ContactoService } from './services/contacto.service';
import {AlumnoService} from './services/alumno.service';
import {ProfesorService} from './services/profesor.service';
import {MateriaService} from './services/materia.service';
import {SeccionService} from './services/seccion.service';
import {SeccionAlumnoService} from './services/seccionAlumno.service';
import {ActividadSeccionService} from './services/actividadSeccion.service';
import {ActividadAlumnoService} from './services/actividadAlumno.service';

//COMPONENTES
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NavbarComponent } from './components/dashboard/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { UsuarioComponent } from './components/dashboard/usuario/usuario.component';
import { ContactoComponent } from './components/dashboard/contacto/contacto.component';
import { ContactoFormComponent } from './components/dashboard/contacto/contacto-form.component';
import { CategoriaComponent } from './components/dashboard/categoria/categoria.component';
import { SignupComponent } from './components/signup/signup.component';
import { DashboardAlumnoComponent } from './components/dashboard-alumno/dashboard-alumno.component';
import { DashboardProfesorComponent } from './components/dashboard-profesor/dashboard-profesor.component';
import { AlumnoComponent } from './components/dashboard/alumno/alumno.component';
import {AlumnoDetalleComponent} from './components/dashboard/alumno/alumno-detalle.component';
import { ProfesorComponent } from './components/dashboard/profesor/profesor.component';
import {ProfesorFormComponent} from './components/dashboard/profesor/profesor-form.component';
import { MateriaComponent } from './components/dashboard/materia/materia.component';
import {MateriaFormComponent} from './components/dashboard/materia/materia-form.component';
import {NavbarAlumnoComponent} from './components/dashboard-alumno/navbar-alumno/navbar-alumno.component';
import {AlumnoAlumnoComponent} from './components/dashboard-alumno/alumno-alumno/alumno-alumno.component';
import {AlumnoAlumnoFormComponent} from './components/dashboard-alumno/alumno-alumno/alumno-alumno-form.component';
import { NavbarProfesorComponent } from './components/dashboard-profesor/navbar-profesor/navbar-profesor.component';
import { ActividadProfesorComponent } from './components/dashboard-profesor/actividad-profesor/actividad-profesor.component';
import {ActividadProfesorFormComponent} from './components/dashboard-profesor/actividad-profesor/actividad-profesor-form.component';
import { ActividadAlumnoComponent } from './components/dashboard-alumno/actividad-alumno/actividad-alumno.component';
import {actividadAlumnoFormComponent} from './components/dashboard-alumno/actividad-alumno/actividad-alumno-form.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UsuarioComponent,
    ContactoComponent,
    CategoriaComponent,
    SignupComponent,
    DashboardComponent,
    NavbarComponent,
    ContactoFormComponent,
    DashboardAlumnoComponent,
    DashboardProfesorComponent,
    AlumnoComponent,
    AlumnoDetalleComponent,
    ProfesorComponent,
    ProfesorFormComponent,
    MateriaComponent,
    MateriaFormComponent,
    NavbarAlumnoComponent,
    AlumnoAlumnoComponent,
    AlumnoAlumnoFormComponent,
    NavbarProfesorComponent,
    ActividadProfesorComponent,
    ActividadProfesorFormComponent,
    ActividadAlumnoComponent,
    actividadAlumnoFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    app_routing
  ],
  providers: [
    UsuarioService,
    AuthGuardService,
    ContactoService,
    AlumnoService,
    ProfesorService,
    MateriaService,
    SeccionService,
    SeccionAlumnoService,
    ActividadSeccionService,
    ActividadAlumnoService
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
