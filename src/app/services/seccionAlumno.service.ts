import {Injectable} from '@angular/core';
import {Http,Headers,RequestOptions} from '@angular/http';
import {UsuarioService} from './usuario.service';
import 'rxjs/Rx';
@Injectable()
export class SeccionAlumnoService{
    url:string = "http://localhost:3000";
    constructor(
        private http:Http,
        private usuarioService:UsuarioService
    ){}
    public getSeccionesAlumno(){ 
        let uri = `${this.url}/api/v1/seccionalumno/`;
        let headers = new Headers({
            'Authorization': this.usuarioService.getToken()
        })
        return this.http.get(uri,{headers})
        .map(res =>{
            return res.json();
        })
    }

        public getSeccionAlumno(idSeccionMA:any){
        console.log(idSeccionMA);
        let uri = `${this.url}/api/v1/seccionalumno/${idSeccionMA}`;
        let headers = new Headers({
            'Authorization':this.usuarioService.getToken()
        });
        return this.http.get(uri,{headers})
        .map(res =>{
            console.log(res.json());
            return res.json();
        })

    }
    public insetSeccionAlumno(seccionAlumno:any){
        let uri = `${this.url}/api/v1/seccionalumno`;
        let headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.usuarioService.getToken()
        });
        let data = JSON.stringify(seccionAlumno);
        return this.http.post(uri,data,{headers})
        .map(res =>{
            return res.json();
        });

    }
    
}