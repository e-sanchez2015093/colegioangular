import {Injectable} from '@angular/core';
import {Http,Headers,RequestOptions} from '@angular/http';
import {UsuarioService} from './usuario.service';
import 'rxjs/Rx';
@Injectable()
export class SeccionService{
    url:string = "http://localhost:3000";
    constructor(
        private http:Http,
        private usuarioService:UsuarioService
    ){}
    public getSecciones(){ 
        let uri = `${this.url}/api/v1/seccion/`;
        let headers = new Headers({
            'Authorization': this.usuarioService.getToken()
        })
        return this.http.get(uri,{headers})
        .map(res =>{
            return res.json();
        })
    }

        public getSeccion(idScceion:any){
        let uri = `${this.url}/api/v1/seccion/${idScceion}`;
        let headers = new Headers({
            'Authorization':this.usuarioService.getToken()
        });
        return this.http.get(uri,{headers})
        .map(res =>{
            console.log(res.json());
            return res.json();
        })

    }
    
}