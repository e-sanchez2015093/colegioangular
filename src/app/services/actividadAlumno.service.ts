import {Injectable} from '@angular/core';
import {Http,Headers,RequestOptions} from '@angular/http';
import {UsuarioService} from './usuario.service';
import 'rxjs/Rx';
@Injectable()
export class ActividadAlumnoService{
    url:string = "http://localhost:3000";
    constructor(
        private http:Http,
        private usuarioService:UsuarioService
    ){}
    public getActividadesAlumno(){ 
        let uri = `${this.url}/api/v1/actividadalumnos/`;
        let headers = new Headers({
            'Authorization': this.usuarioService.getToken()
        })
        return this.http.get(uri,{headers})
        .map(res =>{
            return res.json();
        })
    }

        public getActividadAlumno(idSeccionMA:any){
        console.log(idSeccionMA);
        let uri = `${this.url}/api/v1/actividadalumnos/${idSeccionMA}`;
        let headers = new Headers({
            'Authorization':this.usuarioService.getToken()
        });
        return this.http.get(uri,{headers})
        .map(res =>{
            console.log(res.json());
            return res.json();
        })

    }
    public insetActividadSeccion(actividadSecion:any){
        let uri = `${this.url}/api/v1/actividadalumnos/`;
        let headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.usuarioService.getToken()
        });
        let data = JSON.stringify(actividadSecion);
        console.log(data);
        return this.http.post(uri,data,{headers})
        .map(res =>{
            return res.json();
        });

    }
    public editarActividad(actividad:any,idActividadAlumno:any){
        let uri = `${this.url}/api/v1/actividadalumnos/${idActividadAlumno}`;
        let headers = new Headers({
        'Content-Type': 'application/json',
        'Authorization': this.usuarioService.getToken()
        });
        let data = JSON.stringify(actividad);
        return this.http.put(uri,data,{headers})
        .map(res =>{
            return res.json();
        });
         
    }
    public upload(file:any){
    console.log("Esta en el service");
    console.log("El file: " + file);
    console.log("file name");
    console.log(file.name);
    let uri = "http://localhost:3000/upload";
    let token = localStorage.getItem('token');
    let headers = new Headers({'Accept':'application/json'});
    headers.append('Authorization',token);
    let options = new RequestOptions({'headers':headers});

    let formData = new FormData();
    formData.append('file',file,file.name);
    return this.http.post(uri,formData,options)
      .map(res => {
        return res.json();
      })
    }
    
}