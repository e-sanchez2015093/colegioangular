import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { UsuarioService } from './usuario.service';
import 'rxjs/Rx';
@Injectable()
export class ProfesorService {
    constructor( private http: Http,
    private usuarioService: UsuarioService ) {  }
    public uri: string = "http://localhost:3000/api/v1/profesor/";
    
    public getProfesores() {
        let headers = new Headers({
            'Authorization': this.usuarioService.getToken()
        });
        return this.http.get(this.uri, {headers})
        .map(res => {
            return res.json();
        } )
    }

    getProfesor(idProfesor: any) {
        let headers = new Headers({
      'Authorization': this.usuarioService.getToken()
        });
        let uri = this.uri +idProfesor;
        return this.http.get(uri, {headers})
        .map(res => {
            console.log(res.json());
            return res.json();

        })
    }

    nuevoProfesor(profesor: any){
        let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
        });

        let data = JSON.stringify(profesor);
        return this.http.post(this.uri, data, {headers})
        .map(res => {
            return res.json();
        })
    }

    editarProfesor(profesor: any, idProfesor:any ) {
        let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
        });

        let uri = this.uri + idProfesor;
        let datos = JSON.stringify(profesor);
        return this.http.put(uri, datos, {headers})
        .map(
            res => {
                return res.json();
            }
        )
    }

    eliminarProfesor(idProfesor: any) {
        let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
        });

        let uri = this.uri + idProfesor;
        return this.http.delete(uri, {headers})
        .map(
            res => {
                return res.json();
                
            }
        )
    }

}