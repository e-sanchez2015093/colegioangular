import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { UsuarioService } from './usuario.service';
import 'rxjs/Rx';
@Injectable()
export class MateriaService {
    constructor(private http: Http,
    private usuarioService: UsuarioService ) {}
    public uri: string = "http://localhost:3000/api/v1/materia/";

    public getMaterias() {
        let headers = new Headers({
            'Authorization': this.usuarioService.getToken()
        });

        return this.http.get(this.uri, {headers})
        .map(res => {
                        console.log(res.json());
            return res.json();

        });
    }

    public getMateria(id: any) {
        let headers = new Headers({
            'Authorization': this.usuarioService.getToken()
        });
        console.log(id);
        let uri = this.uri + id
        return this.http.get(uri, {headers})
        .map(res => {
            return res.json();
        });
    }

    public addMateria(materia: any) {
        let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
        });
        let data = JSON.stringify(materia);
        console.log(JSON.stringify(materia));
        return this.http.post(this.uri, data, {headers})
        .map( res => {
            return res.json();
        })
    }

    public editarMateria(materia: any, idMateria: any) {
        let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
        });

        let uri = this.uri + idMateria;
        let data = JSON.stringify(materia);
        return this.http.put(uri, data, {headers})
        .map(res => {
            return res.json();
        });
    }


    public elminarMateria(idMateria: any) {
       let headers = new Headers({
      'Content-Type': 'application/json',
      'Authorization': this.usuarioService.getToken()
        });
        let uri = this.uri + idMateria;
        return this.http.delete(uri, {headers})
        .map(res => {
            return res.json();
        }) 
    }
} 