import { Component, OnInit } from '@angular/core';
import {AlumnoService} from '../../../services/alumno.service';

@Component({
  selector: 'app-alumno',
  templateUrl: './alumno.component.html'
})
export class AlumnoComponent implements OnInit {

  alumnos:any[]=[];
  constructor(
    private alumnoService:AlumnoService
  ) { }

  ngOnInit() {
    this.inicializar();
  }
  private inicializar(){
    this.alumnoService.getAlumnos().subscribe(data =>{
      this.alumnos = data;
      console.log(this.alumnos);
    })
  }
  public borrarAlumno(idAlumno:number){
    console.log(idAlumno);
    this.alumnoService.eliminarAlumno(idAlumno)
    .subscribe(res =>{
      if(res.estado){
        this.inicializar();
      }
    })
  }
  public asignar(idAlumno:number){
    console.log(idAlumno);
  }

}
