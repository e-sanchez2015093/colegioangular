import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {AlumnoService} from '../../../services/alumno.service';
@Component({
    selector:'app-detalleAlumno',
    templateUrl:'./alumno-detalle.component.html',
    styleUrls: ['./alumno-detalle.component.css']
})
export class AlumnoDetalleComponent implements OnInit {
    url:string;
    notificacion:any = {
        estdo:false,
        mensaje:""
    }
    alumno:any = {
        nombre:"",
        apellido:"",
        nick:"",
        contrasena:"",
        idAlumno:0,
        idUsuario:0
    }
    constructor(
        private router:Router,
        private activatedRoute:ActivatedRoute,
        private alumnoService:AlumnoService
    ){
        this.activatedRoute.params.subscribe(params =>{
            this.url = params["idAlumno"];
            if(this.url !== "nuevo"){
                this.alumnoService.getAlumno(this.url)
                .subscribe(c => this.alumno = c);
            }
        })
    }
    ngOnInit(){}
    
    guardarCambios(){
        if(this.url !== "nuevo"){
           console.log(this.alumno);
           this.alumnoService.editarAlumno(this.alumno,this.url)
           .subscribe(res =>{
               this.setNotificacion(res);
           })
        }
    }

    private setNotificacion(notificacion:any) {
    this.notificacion.estado = notificacion.estado;
    this.notificacion.mensaje = notificacion.mensaje;

    setTimeout(() => {
      this.resetNotificacion();
    }, 5000);
  }

    private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }   

}