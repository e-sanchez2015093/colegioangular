
import { Routes } from '@angular/router';

import { UsuarioComponent } from './usuario/usuario.component';
import { ContactoComponent } from './contacto/contacto.component';
import { ContactoFormComponent } from './contacto/contacto-form.component';
import { CategoriaComponent } from './categoria/categoria.component';
import {AlumnoComponent} from './alumno/alumno.component';
import {AlumnoDetalleComponent} from './alumno/alumno-detalle.component';
import {ProfesorComponent} from './profesor/profesor.component';
import {ProfesorFormComponent} from './profesor/profesor-form.component';
import {MateriaComponent} from './materia/materia.component';
import {MateriaFormComponent} from './materia/materia-form.component';
export const dashboard_routes: Routes = [
  { path: 'alumno', component: AlumnoComponent },
  { path: 'alumno/:idAlumno', component: AlumnoDetalleComponent },
  { path: 'profesor', component: ProfesorComponent },
  { path: 'profesor/:idProfesor', component: ProfesorFormComponent },
  { path: 'materia', component: MateriaComponent },
  { path: 'materia/:idMateria', component: MateriaFormComponent },
  //{ path: 'contacto', component: ContactoComponent },
  //{ path: 'contacto/:idContacto', component: ContactoFormComponent },
  //{ path: 'categoria', component: CategoriaComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'alumno' }
];
