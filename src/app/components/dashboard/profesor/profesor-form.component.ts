import { ProfesorService } from '../../../services/profesor.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, FormsModule } from '@angular/forms';

@Component({
  selector: 'app-profesor-form',
  templateUrl: './profesor-form.component.html'
})
export class ProfesorFormComponent implements OnInit {
url:string;
  profesors:any = {
    nombre: '',
    apellido: '',
    direccion: '',
    telefono: ''
  }

   notificacion:any = {
    estado:false,
    mensaje: ""
  }
  constructor( private router:Router,
    private activatedRoute: ActivatedRoute,
    private profesorService: ProfesorService,
    ) 
    {
      
   this.activatedRoute.params.subscribe(params => {
      this.url = params["idProfesor"];
      
      console.log(this.url);
      this.profesorService.getProfesor(this.url)
      .subscribe(c =>  this.profesors = c);
      
      
       
    });
    }
    

    guardarCambios() {
      this.profesorService.editarProfesor(this.profesors, this.url)
      .subscribe(res => {
        console.log(res);
        this.notificacion.estado = true;
        this.notificacion.mensaje = "Se modifico Profesor";
        setTimeout(() => {
      this.resetNotificacion();
    }, 2000);
      this.router.navigate(['/administrador/profesor']);
      });
    }

    private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }

  ngOnInit() {
  }

}