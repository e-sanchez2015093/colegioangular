import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormsModule } from '@angular/forms';
import { ProfesorService } from '../../../services/profesor.service';

@Component({
  selector: 'app-profesor',
  templateUrl: './profesor.component.html'
})
export class ProfesorComponent implements OnInit {
  profesores: any[] = [];
  agregar: boolean = false;
  profesor:any = {
    nombre: '',
    apellido: '',
    direccion: '',
    telefono: '',
    nick: '',
    contrasena: ''
  }
  public notificacion:any = {
    estado:false,
    mensaje: ""
  }

  guardarCambios(){
    this.profesorService.nuevoProfesor(this.profesor)
    .subscribe(
      res => {
        console.log(res);
        this.inicializar();
        this.notificacion.estado = true;
        this.notificacion.mensaje = "Se agrego Profesor";
        setTimeout(() => {
      this.resetNotificacion();
    }, 4000);
        this.agregar = false;
      }
      
    );
  }
  constructor( private profesorService: ProfesorService,
            ) { }

  ngOnInit() {
    this.inicializar();
  }
  private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }
  private inicializar() {
    this.profesorService.getProfesores()
    .subscribe(
      res => {
        this.profesores = res;
      }
    );
  }

  public agre(){
    this.agregar = true;
  }


  public borrarProfesor(id:any) {
    this.profesorService.eliminarProfesor(id)
    .subscribe(
        res => {
          this.inicializar();
        } 
    );
  } 

}
