import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MateriaService } from '../../../services/materia.service';
@Component({
  selector: 'app-materiaForm',
  templateUrl: './materia-form.component.html'
})
export class MateriaFormComponent implements OnInit {
  url:string;
  materia:any = {
    nombre: ''
  }

  notificacion:any = {
    estado:false,
    mensaje: ""
  }

  constructor( private router: Router,
  private activatedRoute: ActivatedRoute,
  private materiaService: MateriaService) {

    this.activatedRoute.params.subscribe(params => {
      this.url = params["idMateria"];

      if(this.url !== "nuevo") {
        console.log(this.url);
        this.materiaService.getMateria(this.url)
        .subscribe(
          c => this.materia = c);
      } else {
        this.resetFormulario();
      }
    });

   }

  ngOnInit() {
  }
  private resetFormulario() {
    this.materia.nombre = ""
  }

  private setNotificacion(notificacion:any) {
    this.notificacion.estado = notificacion.estado;
    this.notificacion.mensaje = notificacion.mensaje;

    setTimeout(() => {
      this.resetNotificacion();
    }, 5000);
  }

  private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }


  guardarCambios() {
    if(this.url === "nuevo") {
      this.materiaService.addMateria(this.materia)
      .subscribe(res => {
        console.log(res);
        this.notificacion.estado = true;
        this.notificacion.mensaje = "Se agrego correctamente";
        setTimeout(() => {
      this.resetNotificacion();
    }, 2000);
        this.resetFormulario();
        this.router.navigate(['/dashboard/materia']);
      });
    } else {
      console.log(this.materia);
      this.materiaService.editarMateria(this.materia, this.url)
      .subscribe(res => {
        console.log(res);
        this.notificacion.estado = true;
        this.notificacion.mensaje = "Se modifico materia";
        setTimeout(() => {
      this.resetNotificacion();
    }, 2000);
        this.router.navigate(['/dashboard/materia']);

      });
    }
  }

}