import { Component, OnInit } from '@angular/core';
import {MateriaService} from '../../../services/materia.service';

@Component({
  selector: 'app-materia',
  templateUrl: './materia.component.html'
})
export class MateriaComponent implements OnInit {
  materias:any[] = [];
  constructor(  private materiaService: MateriaService) { }

  ngOnInit() {
    this.inicializar();
  }

  private inicializar() {
    this.materiaService.getMaterias().subscribe( res => {
      this.materias = res;
    });
  }

  public borrarMateria(idMateria: number) {
    this.materiaService.elminarMateria(idMateria)
    .subscribe(
      res => {
        console.log(res);
        this.inicializar();
      }
    )
  }





}