import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {ActividadAlumnoService} from '../../../services/actividadAlumno.service';

@Component({
  selector: 'app-alumno-alumno-form',
  templateUrl: './actividad-alumno-form.component.html'
})
export class actividadAlumnoFormComponent implements OnInit {
  url:string;
  seccion:any[] = [];
  notificacion:any = {
    estado:false,
    mensaje: ""
  }
  actividad:any = {
    idActividadAlumno:0,
    idAlumno: 0,
    idActividad:0,
    archivo:"",
    Materia:"",
    Actividad:""
  }

  constructor(
    
    private router:Router,
    private activatedRoute: ActivatedRoute,
    private actividadAlumnoService:ActividadAlumnoService) {
    this.activatedRoute.params.subscribe(params => {
      this.url = params["idActividadAlumno"];
       if(this.url !== "nuevo") {
        this.actividadAlumnoService.getActividadAlumno(this.url)
        .subscribe(c => this.actividad = c);
      } else {
        this.resetFormulario();
      }
      });
     }

  ngOnInit() {

  }
    guardarCambios() {
    if(this.url === "nuevo") {
      this.actividadAlumnoService.insetActividadSeccion(this.actividad)
      .subscribe(res => {
        console.log(res);
        this.setNotificacion(res);
        this.resetFormulario();
      });
    } else {
      console.log(this.actividad);
      this.actividadAlumnoService.editarActividad(this.actividad,this.url)
      .subscribe(res =>{
          console.log(res);
          this.setNotificacion(res);
      })

    }
  }
    private setNotificacion(notificacion:any) {
    this.notificacion.estado = notificacion.estado;
    this.notificacion.mensaje = notificacion.mensaje;

    setTimeout(() => {
      this.resetNotificacion();
    }, 5000);
  }
    private resetFormulario() {
    this.actividad.idActividadAlumno=0;
    this.actividad.idAlumno= 0;
    this.actividad.idActividad=0;
    this.actividad.archivo="";
  }
    private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }
  public fileUpload(event){
      console.log("Esta adentro del envento fileUpload");
      console.log(event.target.files[0]);
      this.actividadAlumnoService.upload(event.target.files[0])
      .subscribe(ruta =>{
          console.log(JSON.stringify(ruta));
          this.actividad.archivo = ruta.path
      },error =>{
          console.log('Error: '+error);
      },() =>{
          console.log('Peticion Terminada')
      })
  }


}