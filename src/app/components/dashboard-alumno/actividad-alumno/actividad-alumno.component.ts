import { Component, OnInit } from '@angular/core';
import {ActividadAlumnoService} from '../../../services/actividadAlumno.service';

@Component({
  selector: 'app-actividad-alumno',
  templateUrl: './actividad-alumno.component.html'
})
export class ActividadAlumnoComponent implements OnInit {
  actvidadAlumno:any[]=[];
  constructor(
    private actividadAlumnoSerice:ActividadAlumnoService
  ) { }

  ngOnInit() {
    this.inicializar();

  }
  private inicializar(){
    this.actividadAlumnoSerice.getActividadesAlumno().subscribe(data =>{
      this.actvidadAlumno = data;
      console.log(this.actvidadAlumno);
    })
  }

}
