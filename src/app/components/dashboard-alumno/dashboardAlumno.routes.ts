import { Routes } from '@angular/router';

import { DashboardAlumnoComponent } from './dashboard-alumno.component';
import {AlumnoAlumnoComponent} from './alumno-alumno/alumno-alumno.component';
import {AlumnoAlumnoFormComponent} from './alumno-alumno/alumno-alumno-form.component';
import {ActividadAlumnoComponent} from './actividad-alumno/actividad-alumno.component';
import {actividadAlumnoFormComponent} from './actividad-alumno/actividad-alumno-form.component';
export const dashboard_routesAlumno: Routes = [
  { path: 'alumno', component: AlumnoAlumnoComponent },
  { path: 'actividad', component: ActividadAlumnoComponent },
  { path: 'alumno/:idSeccionMA', component: AlumnoAlumnoFormComponent },
  { path: 'actividad/:idActividadAlumno', component: actividadAlumnoFormComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'alumno' }
];
