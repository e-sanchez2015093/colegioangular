import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {SeccionAlumnoService} from '../../../services/seccionAlumno.service';
import {SeccionService} from '../../../services/seccion.service';

@Component({
  selector: 'app-alumno-alumno-form',
  templateUrl: './alumno-alumno-form.component.html'
})
export class AlumnoAlumnoFormComponent implements OnInit {
  url:string;
  seccion:any[] = [];
  notificacion:any = {
    estado:false,
    mensaje: ""
  }
  parametros:any = {
    token: "",
    idSeccion: 0
  }

  constructor(
    
    private router:Router,
    private activatedRoute: ActivatedRoute,
    private seccionService: SeccionService,
    private seccionAlumnoService:SeccionAlumnoService) {
    this.activatedRoute.params.subscribe(params => {
      this.url = params["idSeccionMA"];

       if(this.url !== "nuevo") {
        this.seccionAlumnoService.getSeccionAlumno(this.url)
        .subscribe(c => this.parametros = c);
      } else {
        this.resetFormulario();
      }
      });
     }

  ngOnInit() {
        this.seccionService.getSecciones().subscribe(data =>{
            this.seccion = data;
            console.log(this.seccion);
        })
  }
    guardarCambios() {
    if(this.url === "nuevo") {
      this.seccionAlumnoService.insetSeccionAlumno(this.parametros)
      .subscribe(res => {
        console.log(res);
        this.setNotificacion(res);
        this.resetFormulario();
      });
    } else {
      console.log(this.parametros);

    }
  }
    private setNotificacion(notificacion:any) {
    this.notificacion.estado = notificacion.estado;
    this.notificacion.mensaje = notificacion.mensaje;

    setTimeout(() => {
      this.resetNotificacion();
    }, 5000);
  }
    private resetFormulario() {
    this.parametros.token = "";
    this.parametros.idSeccion = 0;
  }
    private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }


}