import { Component, OnInit } from '@angular/core';
import {SeccionService} from '../../../services/seccion.service';
import {SeccionAlumnoService} from '../../../services/seccionAlumno.service';
@Component({
  selector: 'app-alumno',
  templateUrl: './alumno-alumno.component.html'
})
export class AlumnoAlumnoComponent implements OnInit {
  seccioneAlumno:any[]=[];
  constructor(
    private seccionAlumnoService:SeccionAlumnoService
  ) { }

  ngOnInit() {
    this.inicializar();
  }
  private inicializar(){
    this.seccionAlumnoService.getSeccionesAlumno().subscribe(data =>{
      this.seccioneAlumno = data;
      console.log(this.seccioneAlumno);
    })
  }
}
