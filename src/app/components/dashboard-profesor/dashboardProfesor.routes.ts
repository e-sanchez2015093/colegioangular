import { Routes } from '@angular/router';


import {DashboardProfesorComponent} from './dashboard-profesor.component'
import {ActividadProfesorComponent} from './actividad-profesor/actividad-profesor.component';
import {ActividadProfesorFormComponent} from './actividad-profesor/actividad-profesor-form.component';

export const dashboard_routesProfesor: Routes = [
  { path: 'actividad', component: ActividadProfesorComponent },
  { path: 'actividad/:idActividad', component: ActividadProfesorFormComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'actividad' }
];