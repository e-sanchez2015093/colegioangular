import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute} from '@angular/router';
import {ActividadSeccionService} from '../../../services/actividadSeccion.service';
import {SeccionService} from '../../../services/seccion.service';
import {MateriaService} from '../../../services/materia.service';


@Component({
  selector: 'app-profesor-profesor-form',
  templateUrl: './actividad-profesor-form.component.html'
})
export class ActividadProfesorFormComponent implements OnInit {
  url:string;
  seccion:any[] = [];
  materia:any[] = [];
  notificacion:any = {
    estado:false,
    mensaje: ""
  }
  actividad:any = {
    idMateria: 0,
    descripcion:"",
    fecha_final:"",
    bimestre:0,
    valor:0,
    idSeccion: 0
  }

  constructor(
    
    private router:Router,
    private activatedRoute: ActivatedRoute,
    private seccionService: SeccionService,
    private materiaServie:MateriaService,
    private actividadSeccion:ActividadSeccionService) {
    this.activatedRoute.params.subscribe(params => {
      this.url = params["idActividad"];

       if(this.url !== "nuevo") {
        this.actividadSeccion.getActividadSeccion(this.url)
        .subscribe(c => this.actividad = c);
      } else {
        this.resetFormulario();
      }
      });
     }

  ngOnInit() {
        this.seccionService.getSecciones().subscribe(data =>{
            this.seccion = data;
            console.log(this.seccion);
        });
        this.materiaServie.getMaterias().subscribe(data =>{
            this.materia = data;
            console.log(this.materia);
        })
  }
    guardarCambios() {
    if(this.url === "nuevo") {
      console.log(this.actividad);
      this.actividadSeccion.insetActividadSeccion(this.actividad)
      .subscribe(res => {
        console.log(res);
        this.setNotificacion(res);
        this.resetFormulario();
      });
    } else {
      console.log(this.actividad);

    }
  }
    private setNotificacion(notificacion:any) {
    this.notificacion.estado = notificacion.estado;
    this.notificacion.mensaje = notificacion.mensaje;

    setTimeout(() => {
      this.resetNotificacion();
    }, 5000);
  }
    private resetFormulario() {

    this.actividad.idMateria = 0;
    this.actividad.descripcion="";
    this.actividad.fecha_final=0  ;
    this.actividad.bimestre=0;
    this.actividad.valor=0;
    this.actividad.idSeccion= 0
  }
    private resetNotificacion() {
    this.notificacion.mensaje = "";
    this.notificacion.estado = false;
  }


}