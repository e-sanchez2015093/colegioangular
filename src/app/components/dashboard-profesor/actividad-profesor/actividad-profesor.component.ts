import { Component, OnInit } from '@angular/core';
import {ActividadSeccionService} from '../../../services/actividadSeccion.service';
@Component({
  selector: 'app-actividad-profesor',
  templateUrl: './actividad-profesor.component.html'
})
export class ActividadProfesorComponent implements OnInit {
  actividadSeccion:any[]=[];
  constructor(
    private actividadSeccionService:ActividadSeccionService
  ) { }

  ngOnInit() {
    this.inicializar();
  }

  private inicializar(){
    this.actividadSeccionService.getActividadesSeccion().subscribe(data =>{
      this.actividadSeccion = data ;
      console.log(this.actividadSeccion);
    })
  }

}
