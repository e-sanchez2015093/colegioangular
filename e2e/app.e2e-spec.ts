import { ProyectoPruebaPage } from './app.po';

describe('proyecto-prueba App', () => {
  let page: ProyectoPruebaPage;

  beforeEach(() => {
    page = new ProyectoPruebaPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
